
// It allows to write global logis such as verify that madnatory event field exist
// timestamp is very important at the AbstractEvent level so that we can apply retry logic whenever needed
// try is true or false/undefined. If retry is set to true, there must be a period in hours to decide how to long we need to keep retying
//          For retyr, we also need to have a default frequency unless a specific frequency is passed when the event is produced
class AbstractEvent{
    constructor(retry=false, retryFrequency=3 ){
        this.created= new Date().getTime();
        this.retry = retry;
        this.retryFrequency = retryFrequency;
    }
}

module.exports = AbstractEvent;




