const event_processing = require('events_processing_module/producers/retry')
const TimeoutPromise = require('events_processing_module/lib')
const logEventFalure = require('./logEventFailure')
const AWS = require('aws-sdk');

AWS.config.update({
  accessKeyId: 'your_access_key',
  secretAccessKey: 'your_secret_key',
  region: 'your_region',
});


// Create an SQS instance
const sqs = new AWS.SQS();

// Replace with your SQS queue URL
const queueUrl = 'https://sqs.ADNOC_region.amazonaws.com/<accountID>/retry/';

class ConsumeRetry {

  constructor(listener, className, delay, period) {
    this.listener = listener
    this.className = className;
    this.delay = delay;
    // number of hours that retry should be attempted
    this.period = period;
    this.queueUrl = null;
    const params = {
      QueueName: className, // The name of the queue to create
      Attributes: {
        'DelaySeconds': '20', // The number of seconds to delay the delivery of new messages
        'VisibilityTimeout': '60' // The number of seconds during which the message is hidden from other consumers
      }
    };
    
  sqs.getQueueUrl({ QueueName: queueName }).promise()
    .then((data) => {
      console.log(`Queue URL: ${data.QueueUrl}`);
      this.queueUrl = queue.QueueUrl;
      setInterval(this.process(), 60000);
    })
    .catch((error) => {
      if (err.code === 'AWS.SimpleQueueService.NonExistentQueue') {
        return sqs.createQueue({ QueueName: queueName }).promise()
          .then((data) => {
            this.queueUrl = data.QueueUrl;
            setInterval(this.process(), 60000);
            
          });
      }
      console.error(error);
    });


  }

  logEvnetFalure(message) {
    // logs this.event to a Dynamo DB failures table. This table should have at least the following columns:
    // customer log ID if any
    // class_name is the event class name
    // event_timesamp is the original event time_stamp
    // timeStamp is the event log timestamp in this table
    // data is the event body


    // it should call a DB method, library or class to write to the table
    logEventFalure({
      className : this.className,
      eventTimeStamp : message.created,
      createdAt : new Date().toISOString(),
      data : message
    })
  }

  retry(event) {
    this.produceRetry(event, this.delay);
  }

  async process() {
    const message = await this.receiveEvent()
    if(message){

    const messagePromise = TimeoutPromise(35*1000, async (resolve, reject) => {
        try {
          
          console.log('Message received:', message.Body);
          await that.listener(message.Body);
          resolve(true);
          
        }
        catch(e){
          reject(e)
        }
    });
    
    messagePromise.then(() => {
      // Delete the message after processing
      deleteMessage(message.ReceiptHandle);
    });
    messagePromise.catch(() => {
      // check the period to stop retrying after like a N number of hours
      if(message.Body.created + (this.period  * 60 * 60 * 1000) > new Date().getTime() ) {
        this.logEvnetFalure(message)
      }else {
        deleteMessage(message.ReceiptHandle)
        that.retry()
      }
    }) 
  }
  }

  async produceRetry(event, delay) {

  

    const params = {
      MessageBody: event,
      QueueUrl: this.queueUrl,
    };
  
    (delay) && (params.DelaySeconds = delay)
  
    try {
      const result = await sqs.sendMessage(params).promise();
      console.log('Message sent:', result.MessageId);
    } catch (error) {
      console.error('Error sending message:', error);
      //log this message to the dynamo Db 
    }
  }

  async receiveEvent() {

    const that = this;
    const event_url = this.queueUrl;
  
    const params = {
      QueueUrl: queueUrl,
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 60, // seconds - how long to hide the message from other consumers
      WaitTimeSeconds: 5, // seconds - how long to wait for a message to arrive
    };
  
    try {
      const result = await sqs.receiveMessage(params).promise();
      

      if (result.Messages) {
        // edge case to be handled in case the array has no elements
        return result.Messages[0]
        
        
      } else {
        console.log('No messages received');
      }
    } catch (error) {
      console.error('Error receiving message:', error);
    }
  }
  
  async deleteMessage(receiptHandle) {
    const params = {
      QueueUrl: this.queueUrl,
      ReceiptHandle: receiptHandle,
    };
  
    try {
      await sqs.deleteMessage(params).promise();
      console.log('Message deleted:', receiptHandle);
    } catch (error) {
      console.error('Error deleting message:', error);
    }
  }
  

}




// delay in seconds



