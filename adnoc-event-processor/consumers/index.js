const { Kafka } = require('kafkajs');
const ConsumeRetry = require('retry');

// read from auto registry or settings
// centralize kafak connection for consumers and producers
const kafka = new Kafka({ clientId: 'my-app', brokers: ['boot-ajiwg4a1.c1.kafka-serverless.eu-west-1.amazonaws.com:9098'] });

class EventConsumer{
constructor(event_class, listener_func, options){
    // need to pass group ID string if it is passed in options
    //const consumer = kafka.consumer({ groupId: options.group_id });
    this.consumer = kafka.consumer();
    this.event_class = event_class
    this.listener = listener_func

    if (options.retry)
      this.retry = retry
      this.consumeRetry = new ConsumeRetry(
        listener_func,
        event_class,
        options ? options.delay : 15 * 60 * 1000,
        options ? options.peroid : 24,
      )

  }

  async run(){
    const that = this;
    const event_class = this.event_class;
    await this.consumer.subscribe({ topic: this.event_class })
    await this.consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
      const event = JSON.parse(message.value.toString());
      try{
          // Need to do it inside a promise that times out just in case the listener hangs
          that.listener(event)
      }catch(e){
        // do some logging
        if (that.retry) {
          that.retry()
        }
      }
      },
    });
  }

  retry(event) {
    // throws to a retry queue or topic to keep retrying until retry time expiry
    // it uses the same listener passed in the EventConsumer constructor
    // it can introduce a delay by using SQS native delay feature
    // when retry expires and failure is still there, it logs the failed events to a DynamoDB table
    this.consumeRetry.retry(event)

  }

}
