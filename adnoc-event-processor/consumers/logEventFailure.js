const AWS = require('aws-sdk');

const documentClient = new AWS.DynamoDB.DocumentClient();


module.exports = function logEventFalure(item){
    const params = {
        TableName: 'events-logs',
        Item: item
      };
      documentClient.put(params, (error, data) => {
        if (error) {
          console.error(`Failed to write to DynamoDB: ${error.message}`);
        } else {
          console.log('Data written to DynamoDB:', data);
        }
      });
}



