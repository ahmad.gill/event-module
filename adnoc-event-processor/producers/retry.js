const AWS = require('aws-sdk');

AWS.config.update({
  accessKeyId: 'your_access_key',
  secretAccessKey: 'your_secret_key',
  region: 'your_region',
});


// Create an SQS instance
const sqs = new AWS.SQS();

// Replace with your SQS queue URL
const queueUrl = 'https://sqs.ADNOC_region.amazonaws.com/<accountID>/retry/';

// delay in seconds
async function produceRetry(event, delay) {

  const event_url = queueUrl + event.class_name;

  const params = {
    MessageBody: event,
    QueueUrl: event_url,
  };

  (delay) && (params.DelaySeconds = delay)

  try {
    const result = await sqs.sendMessage(params).promise();
    console.log('Message sent:', result.MessageId);
  } catch (error) {
    console.error('Error sending message:', error);
  }
}


