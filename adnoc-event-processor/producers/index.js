const { Kafka } = require('kafkajs');

// read from auto registry or settings
const kafka = new Kafka({ clientId: 'my-app', brokers: ['boot-ajiwg4a1.c1.kafka-serverless.eu-west-1.amazonaws.com:9098'] });

async function produceEvent(event) {
  const topic = event.constructor.name; 
  const eventPayload = JSON.stringify(event);
  console.log(topic, "--- Topic")
  const producer = kafka.producer();
  await producer.connect();
  await producer.send({
    topic,
    messages: [{ value: eventPayload }],
  });
  await producer.disconnect();
}


module.exports = produceEvent