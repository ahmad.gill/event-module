const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const produceEvent = require('./adnoc-event-processor/producers')
const TestEvent = require('./adnoc-event-processor/events/test')
const app = new Koa();
const router = new Router();
const {
  createMechanism
} = require('@jm18457/kafkajs-msk-iam-authentication-mechanism')


// Use bodyParser middleware to parse request bodies
app.use(bodyParser());

// Define GET endpoint at root path
router.get('/', async ctx => {
  ctx.body = 'Hello, World!';
});

// Define POST endpoint at /users path
router.post('/users', async ctx => {
  const { name, email } = ctx.request.body;
  ctx.body = `Creating user ${name} with email ${email}`;
});

// Use router middleware
app.use(router.routes());
const sasl_param = {
  ...createMechanism({region: 'eu-west-1'}),
  jaas : {
    config : 'software.amazon.msk.auth.iam.IAMLoginModule required'
  },
  client : {
    callback : {
      handler : {
        class : 'software.amazon.msk.auth.iam.IAMClientCallbackHandler'
      }
    }
  }
}

// console.log(sasl_param)
const { Kafka } = require('kafkajs')
// console.log(createMechanism({ region: 'eu-west-1' }))
const kafka = new Kafka({
  clientId: 'my-app',
  logLevel : 'info',
  brokers: ['boot-ajiwg4a1.c1.kafka-serverless.eu-west-1.amazonaws.com:9098'],
  authenticationTimeout: 100000,
  ssl: true,
  // sasl: {
  //   mechanism: 'AWS', 
  //   // mechanism: 'plain', // scram-sha-256 or scram-sha-512
  //   // username: 'my-username',
  //   // password: 'my-password'
  //   // region: 'eu-west-1',
  //   // iamRoleArn: 'arn:aws:iam::717645410404:role/development-ec2',
  //   // =authenticationTimeout: 10000,
  //   authorizationIdentity: 'AIDA2OFX76BSFOSKNJED3', // UserId or RoleId
  //   accessKeyId: 'AKIA2OFX76BSCRDSXUOQ',
  //   secretAccessKey: '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',
  //   // jaas : {
  //   //       config : 'software.amazon.msk.auth.iam.IAMLoginModule required'
  //   //     }
  // }
  sasl : sasl_param
})

const producer = kafka.producer({
    allowAutoTopicCreation: true,
})
const consumer = kafka.consumer({ groupId: 'my-group' })
const consumer1 = kafka.consumer({ groupId: 'my-group' })

const run = async () => {
  await producer.connect()
  await consumer.connect()
  await consumer1.connect()
  await consumer.subscribe({ topic: 'dev-wash-go' })
//   await consumer.subscribe({ topic: 'my-topic' })
console.log(consumer)
consumer.on('consumer.crash', (error, groupId) => {
  console.error(`Consumer with groupId ${groupId} has crashed: ${error}`)
})

consumer.on('consumer.disconnect', () => {
  console.log('Consumer has disconnected from the broker')
})

consumer.on('consumer.crash', (error, groupId) => {
  console.error(`Consumer with groupId ${groupId} has crashed: ${error}`)
})

consumer.on('consumer.disconnect', () => {
  console.log('Consumer has disconnected from the broker')
})


  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log("@@@@@@@@@@@@@")
      console.log({
        value: message.value.toString(),
        key: message.key.toString(),
        headers: message.headers,
        topic,
        partition,
        offset: message
      })
      if(message.key.toString() == "Test"){
        throw new Error("Throwing new error")
        console.log("Consuming message")
      }
      
      try{
        
      }catch(err){
        console.log("Printing out error")
      }
      
    },
  })
  for(let i=0;i<12;i++){
    console.log("+++++++++")
    await producer.send({
        topic: 'dev-wash-go',
        messages: [
          {
             value: 'Hello Kafka!123',
            key : "Test"
        }
        ]
      })
  }
}

run().catch(console.error)



// Start the server listening on port 3000
app.listen(3000, () => {
  console.log('Server listening on port 3000');
});

